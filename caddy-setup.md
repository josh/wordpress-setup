Septemeber 6th

I followed these instructions to setup Caddy: https://git.daplie.com/Daplie/sysadmin-docs/blob/master/Caddy-systemd.md

Then I created a log directory: `sudo mkdir /var/log/caddy`

Then I set permissions for it: `sudo chown -R www-data:www-data /var/log/caddy`

I used this configuration for Caddy:

```
bereanbibleutah.org {

log /var/log/caddy/berean.log

proxy / 127.0.0.1:8080 {
        fail_timeout 0s

        header_upstream Host {host}
        header_upstream X-Real-IP {remote}
        header_upstream X-Forwarded-For {remote}
        header_upstream X-Forwarded-Proto {scheme}
        header_upstream X-Forwarded-Ssl on
}

}

bereanbibleutah.com {

redir https://bereanbibleutah.org{uri}

}

```

Changes undone. Caddy doesn't work.